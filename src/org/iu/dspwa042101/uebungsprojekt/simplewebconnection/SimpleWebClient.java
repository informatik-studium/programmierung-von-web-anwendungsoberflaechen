package org.iu.dspwa042101.uebungsprojekt.simplewebconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class SimpleWebClient {

    public static void main(String[] args) {
        try {
            // Send a GET request to the server
            Socket socket = new Socket("localhost", 80);
            PrintStream ps = new PrintStream(socket.getOutputStream(), true);
            ps.println("GET / HTTP/1.1");
            ps.println("Host: localhost");
            ps.println();

            // Debug the response from the server
            System.out.println("+++ Sent request, logging response +++");
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

            // Close the socket connection
            socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
